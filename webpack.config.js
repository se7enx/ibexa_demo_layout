const Encore = require('@symfony/webpack-encore');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const FractalWebpackPlugin = require('fractal-webpack-plugin');
const WebpackImportGlobLoader = require('webpack-import-glob-loader');
const WebpackShellPlugin = require('webpack-shell-plugin');
const path = require('path');


Encore.reset();
Encore.setOutputPath('dist')
    .setPublicPath('/')
    .enableSassLoader()
    .enableSingleRuntimeChunk()
    .addPlugin(
        new WebpackShellPlugin({onBuildStart:['bin/console bazinga:js-translation:dump dist --merge-domains']})
    )
    .addPlugin(
            new FractalWebpackPlugin({
            mode: Encore.isProduction() ? 'export' : 'server',
            configPath: './fractal.config.js',
            })
        )
    .copyFiles({
        from: './src/assets/svg',
        to: './images/[path][name].[ext]',
        includeSubdirectories: true,
        pattern: /.*/
    })
    .addPlugin(
        new CopyWebpackPlugin({
            patterns: [{from: 'src/assets/images/', to: './images/[path][name].[ext]'}]
        }),
    )
    .addPlugin(new ImageminPlugin())

Encore.addEntry('welcome_page', [
    path.resolve(__dirname, './src/assets/scss/welcome_page.scss'),
]);

// Put your config here.
Encore.addEntry('app_js', [
    path.resolve(__dirname, './src/assets/app.js'),
]);

Encore.addEntry('app_styles', [
    path.resolve(__dirname, './src/assets/styles/app.css'),
]);

module.exports = Encore.getWebpackConfig();

module.exports.module.rules.push(      {
    test: /\.js$/,
    use: 'webpack-import-glob-loader'
});

module.exports.module.rules.push(      {
    test: /\.scss$/,
    use: 'webpack-import-glob-loader'
});